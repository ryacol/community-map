### Conversation

<!-- What's the story? 

As a <persona/user/system>
I want <desired behaviour/output>
so that <the product achieves some outcome -->

### Scenarios

<!-- What scenarios describe better the expected behaviour once this story is implemented?

Given <context>
When <event>
Then <should behave like...>

Gerkhin template is described at [Dan North Blog](https://dannorth.net/introducing-bdd/) -->

### UX/UI Details

<!-- Include images, icons and references to external sources like Zeplin -->
