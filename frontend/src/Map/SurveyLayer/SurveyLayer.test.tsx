/* eslint @typescript-eslint/no-empty-function: 0 */
// Because the Mock functions are meant to be empty

import React from "react"
import { SurveyLayer } from "./SurveyLayer"
import { shallow } from "enzyme"

function MockSource() {}

function MockLayer() {}

jest.mock("react-mapbox-gl", () => ({
    Source: MockSource,
    Layer: MockLayer,
}))

function getShallowWrapper(fileIdentifier: string) {
    return shallow(
        <SurveyLayer
            surveyDataType=""
            range={{ start: 0, end: 0 }}
            riskDomains={[]}
            fileIdentifier={fileIdentifier}
            maxRange={10}
            sourceLayer=""
            surveyDomain=""
        />,
    )
}

function getExpectedSourceProps(fileIdentifier: string) {
    return {
        id: `source_${fileIdentifier}`,
        tileJsonSource: {
            type: "vector",
            tiles: [`http://localhost/data/${fileIdentifier}/{z}/{x}/{y}.pbf`],
        },
    }
}

describe("<SurveyLayer />", () => {
    describe("- <Source />", () => {
        it("exists", () => {
            const wrapper = getShallowWrapper("deprivation_uk")
            expect(wrapper.find(MockSource).length).toEqual(1)
        })

        it("has the correct props for a Survey Data Type", () => {
            const expectedProps = getExpectedSourceProps("deprivation_uk")
            const wrapper = getShallowWrapper("deprivation_uk")
            expect(wrapper.find(MockSource).props()).toEqual(expectedProps)
        })
    })

    describe("- <Layer />", () => {
        it("exists", () => {
            const wrapper = getShallowWrapper("deprivation_uk")
            expect(wrapper.find(MockLayer).length).toEqual(1)
        })
    })

    it("has a matching source id to the layer's sourceId", () => {
        const wrapper = getShallowWrapper("deprivation_uk")
        const sourceId = wrapper.find(MockSource).props().id
        const layerSourceId = wrapper.find(MockLayer).props().sourceId
        expect(sourceId === layerSourceId).toBe(true)
    })
})
