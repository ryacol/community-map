export const mapboxConfig = {
    // Note: This variable will still be baked into the app bundle at build time and thus be accessible
    // by any app user. It is assumed that the application is exclusively served to authorised clients.
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    accessToken: process.env.REACT_APP_MAPBOX_ACCESS_TOKEN!,
    maxZoom: 12,
    minZoom: 5,
    // If true , the map's canvas can be exported to a PNG using map.getCanvas().toDataURL()
    preserveDrawingBuffer: true,
}
