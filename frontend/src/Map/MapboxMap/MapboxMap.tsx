import React, { useContext } from "react"
import "./MapboxMap.css"
import ReactMapboxGl, { Layer } from "react-mapbox-gl"
import { mapboxConfig } from "./mapboxConfig"
import { ResourceLayer } from "../ResourceLayer/ResourceLayer"
import { extractSelectedOptions } from "../../Shared/Utils/extractFilterOptions"
import { MapboxContext } from "../MapboxContext"
import { SurveyContext } from "../../Surveys/SurveyContext"
import SurveyLayer from "../SurveyLayer/SurveyLayer"
import { ResourceContext } from "../../Resources/ResourceContext"
import { BoundaryLayer } from "../BoundaryLayer/BoundaryLayer"
import { BoundaryContext } from "../../Boundaries/BoundaryContext"

const Map = ReactMapboxGl(mapboxConfig)

const center: [number, number] = [-4, 54.3]
const zoom: [number] = [6]

export function MapboxMap() {
    /*
    Note: Currently unused. Can be handed down and used inside SurveyLayer to obtain data on hovering over the map.

    const [selectedFeature, setSelectedFeature] = useState<any>(undefined)
    const handleMouseMoveOverMap = (ev: any) => {
        setSelectedFeature(ev.features[0].properties)
    }
    const handleMouseLeaveOverMap = () => {
        setSelectedFeature(undefined)
    }
    */

    const { resources } = useContext(ResourceContext)
    const { menu, tab, menuData, tabData, ...resourceData } = resources
    const { boundary } = useContext(BoundaryContext)
    const { setMap } = useContext(MapboxContext)
    const {
        range: surveyRange,
        dataType: surveyDataType,
        maxRange,
        sourceLayer,
        fileIdentifier,
        domain: surveyDomain,
        riskDomains,
    } = useContext(SurveyContext)

    const getFilter = (obj: any) => {
        if (!obj || Object.keys(obj).length === 0) return []
        const options = Object.entries(obj).filter(([key, _]) => obj["dataWanted"].includes(key))
        return getIntersectionFilterForOptions(...options)
    }

    return (
        <Map
            onStyleLoad={(map: any) => {
                setMap(map)
            }}
            className="map"
            center={center}
            zoom={zoom}
            // eslint-disable-next-line react/style-prop-object
            style="mapbox://styles/mapbox/streets-v9"
        >
            <>
                <Layer id={"layer_first"} before={"layer_second"} />
                <Layer id={"layer_second"} />

                {!!surveyDataType && (
                    <SurveyLayer
                        maxRange={maxRange}
                        surveyDomain={surveyDomain}
                        sourceLayer={sourceLayer}
                        fileIdentifier={fileIdentifier}
                        range={surveyRange}
                        riskDomains={extractSelectedOptions(riskDomains)}
                        surveyDataType={surveyDataType}
                    />
                )}

                {boundary && boundary.fileIdentifier && <BoundaryLayer />}

                {Object.values(resourceData).map(
                    (resource: any) =>
                        !!resource && (
                            <ResourceLayer
                                image={getIndicatorImage(require("../../Core/Config/Pins/" + resource["icon"]))}
                                dataLabel={resource["dataLabel"]}
                                filter={getFilter(resource)}
                                key={resource.dataLabel}
                            />
                        ),
                )}
            </>
        </Map>
    )
}

export const getIndicatorImage = (src: string): HTMLImageElement => {
    const indicator: HTMLImageElement = new Image()
    indicator.src = src
    indicator.width = 23
    indicator.height = 30
    return indicator
}

export const getIntersectionFilterForOptions = (...keysWithOptions: [string, any][]) => {
    const keys = keysWithOptions.map(([key, _]) => key)
    const options = keysWithOptions.map(([_, options]) => options)
    const filters = keys.map((key, i) => ["in", key, ...extractSelectedOptions(options[i])])
    return ["all", ...filters]
}

export default MapboxMap
