import React from "react"
import { getIndicatorImage, getIntersectionFilterForOptions } from "./MapboxMap"

jest.mock("react-mapbox-gl", () => (_element: any) => {
    return (props: any) => <div>{props.children}</div>
})

describe("Filter", () => {
    it("should create expected filter from context of filter options", () => {
        const division = { A: true, B: true }
        const directorate = { C: true }

        const result = getIntersectionFilterForOptions(["Division", division], ["Directorate", directorate])
        const resultWithReversedArgs = getIntersectionFilterForOptions(
            ["Directorate", directorate],
            ["Division", division],
        )

        for (const actual of [result, resultWithReversedArgs]) {
            expect(actual.length).toEqual(3)
            expect(actual[0]).toEqual("all")
            expect(actual).toContainEqual(["in", "Division", "A", "B"])
            expect(actual).toContainEqual(["in", "Directorate", "C"])
        }
    })
})

describe("IndicatorImages", () => {
    it("should create an HTMLImageElement with the expected image src", () => {
        const actual = getIndicatorImage("foo")
        const expected = new Image()
        expected.src = "foo"
        expected.width = 23
        expected.height = 30
        expect(actual).toEqual(expected)
    })
})
