import "./Snackbar.css"
import React, { useContext } from "react"
import { SnackbarContext } from "./SnackbarContext"

export const Snackbar = () => {
    const { message, isVisible } = useContext(SnackbarContext)

    return (
        <div id="snackbar" className={isVisible ? "show" : ""}>
            {message}
        </div>
    )
}
