import "./CheckButtonList.css"

import { CheckButton } from "../CheckButton/CheckButton"
import React from "react"
import { ListButtonOption } from "../ListButtonOption/ListButtonOption"

interface CheckButtonsProps<T> {
    title: string
    options: ListButtonOption<T>[]
    onClick: (value: T) => void
    selectedValues: T[]
    color: string
}

export function CheckButtonList<T>(props: CheckButtonsProps<T>) {
    const { selectedValues, onClick, color } = props

    return (
        <div className="check-button-list">
            <h3 className="check-button-title">{props.title}</h3>
            {props.options.map(option => (
                <CheckButton
                    key={option.label}
                    option={option}
                    onClick={onClick}
                    isSelected={selectedValues.includes(option.value)}
                    color={color}
                />
            ))}
        </div>
    )
}
