import React from "react"
import { mount } from "enzyme"
import { ClearButton } from "./ClearButton"

describe("<ClearButton/>", () => {
    it("should visually indicate when no filters are selected", () => {
        const wrapper = mount(<ClearButton clearFunctions={[]} filterCounts={[0]} />)
        const clearButtonText = wrapper.find(".clear-button")
        expect(clearButtonText.hasClass("inactive")).toEqual(true)
    })

    it("should visually indicate when filters are selected", () => {
        const wrapper = mount(<ClearButton clearFunctions={[]} filterCounts={[1]} />)
        const clearButtonText = wrapper.find(".clear-button")
        expect(clearButtonText.hasClass("inactive")).toEqual(false)
    })

    it("should call clear functions on click", () => {
        const mockFunction = jest.fn()
        const wrapper = mount(<ClearButton clearFunctions={[mockFunction]} filterCounts={[1]} />)
        wrapper.find(".clear-button").simulate("click")
        expect(mockFunction).toHaveBeenCalled()
    })
})
