import "./ClearButton.css"

import React from "react"

type returnsVoidType = () => void

interface ClearButtonProps {
    clearFunctions: returnsVoidType[]
    filterCounts: number[]
}

export function ClearButton(props: ClearButtonProps) {
    const isActive = props.filterCounts.reduce((a, b) => a + b) > 0

    const clearFilters = () => {
        props.clearFunctions.forEach(f => f())
    }

    const className = isActive ? "" : "inactive"
    return (
        <div className="clear-button-container" onClick={clearFilters}>
            <span className={`clear-button ${className}`}>Clear filters</span>
        </div>
    )
}
