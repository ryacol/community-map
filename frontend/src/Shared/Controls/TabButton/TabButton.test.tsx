import React from "react"
import { mount, shallow } from "enzyme"
import { TabButton } from "./TabButton"

describe("<TabButton/>", () => {
    it("should display the given text", () => {
        const expectedOption = { label: "Staff", value: 0 }
        const wrapper = mount(<TabButton option={expectedOption} isSelected={false} onClick={() => {}} />)
        const tabButtonText = wrapper.find(".tab-button").text()
        expect(tabButtonText).toEqual(expectedOption.label)
    })

    it("should not have the selected class if it is not selected", () => {
        const expectedIsSelected = false
        const tabButton = shallow(
            <TabButton option={{ label: "Staff", value: 0 }} isSelected={expectedIsSelected} onClick={() => {}} />,
        )
        const tabButtonIsSelected = tabButton.hasClass("selected")
        expect(tabButtonIsSelected).toEqual(expectedIsSelected)
    })

    it("should have the selected class if it is selected", () => {
        const expectedIsSelected = true
        const tabButton = shallow(
            <TabButton option={{ label: "Staff", value: 0 }} isSelected={expectedIsSelected} onClick={() => {}} />,
        )
        const tabButtonIsSelected = tabButton.hasClass("selected")
        expect(tabButtonIsSelected).toEqual(expectedIsSelected)
    })

    it("should call the onClick method with given text", () => {
        const expectedOption = { label: "Staff", value: 0 }
        const mockOnClick = jest.fn()

        const wrapper = shallow(<TabButton option={expectedOption} isSelected={false} onClick={mockOnClick} />)
        wrapper.simulate("click")

        expect(mockOnClick).toHaveBeenCalledWith(expectedOption.value)
    })
})
