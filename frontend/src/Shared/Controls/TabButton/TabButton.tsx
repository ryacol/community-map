import "./TabButton.css"

import React from "react"
import { ListButtonOption } from "../ListButtonOption/ListButtonOption"

interface TabButtonProps<T> {
    option: ListButtonOption<T>
    isSelected: boolean
    onClick: (value: T) => void
}

export function TabButton<T>(props: TabButtonProps<T>) {
    const { option, onClick, isSelected } = props

    const className = `tab-button ${isSelected ? "selected" : ""}`
    return (
        <div className={className} onClick={() => onClick(option.value)}>
            {option.label}
        </div>
    )
}
