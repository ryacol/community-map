import "./DropdownItem.css"
import React from "react"

interface DropdownItemProps {
    onClick: () => void
    label: string
    disabled?: boolean
    selected?: boolean
}

export const DropdownItem = (props: DropdownItemProps) => {
    const { disabled, label, onClick, selected } = props
    const className = `dropdown-item ${selected ? "selected" : ""}`
    return (
        <button onClick={onClick} className={className} disabled={disabled}>
            {label}
        </button>
    )
}
