import "./BackButton.css"

import React from "react"
import BackIcon from "./BackIcon.svg"

interface BackButtonProps {
    onClick: () => void
}

export function BackButton(props: BackButtonProps) {
    return (
        <div className="back-button" onClick={() => props.onClick()}>
            <img src={BackIcon} alt="back icon" aria-hidden="true" />
            <div className="back-button-label">Back</div>
        </div>
    )
}
