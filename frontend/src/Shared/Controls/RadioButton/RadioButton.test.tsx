import { shallow } from "enzyme"
import React from "react"
import { RadioButton } from "./RadioButton"
import { ListButtonOption } from "../ListButtonOption/ListButtonOption"

function shallowRadioButton<T>(
    option: ListButtonOption<T>,
    isSelected = false,
    mockOnClick: (value: T) => void = jest.fn,
) {
    return shallow(<RadioButton option={option} isSelected={isSelected} onClick={mockOnClick} color="teal" />)
}

const defaultOption = { label: "some-text", value: "some-value" }

describe("<RadioButton/>", () => {
    it("should display the label given to it", () => {
        const expectedText = "Some text for a label"
        const wrapper = shallowRadioButton({ label: expectedText, value: "some-value" })

        const labelText = wrapper.text()
        expect(labelText).toEqual(expectedText)
    })

    it("should be the given color when selected", () => {
        const wrapper = shallowRadioButton(defaultOption, true)

        const radioButtonInner = wrapper.find(".rb-inner")
        const radioButtonOuter = wrapper.find(".rb-outer")

        expect(radioButtonInner.hasClass("teal")).toBe(true)
        expect(radioButtonOuter.hasClass("teal")).toBe(true)
    })

    it("should not add the selected class to the button if it is not selected", () => {
        const wrapper = shallowRadioButton(defaultOption, false)

        const radioButtonInner = wrapper.find(".rb-inner")
        const radioButtonOuter = wrapper.find(".rb-outer")

        expect(radioButtonInner.hasClass("selected")).toBe(false)
        expect(radioButtonOuter.hasClass("selected")).toBe(false)
    })

    it("should add the selected class to the button if it is selected", () => {
        const wrapper = shallowRadioButton(defaultOption, true)

        const radioButtonInner = wrapper.find(".rb-inner")
        const radioButtonOuter = wrapper.find(".rb-outer")

        expect(radioButtonInner.hasClass("selected")).toBe(true)
        expect(radioButtonOuter.hasClass("selected")).toBe(true)
    })

    it("should call the onClick function with the option's value when clicked", () => {
        const expectedValue = 1
        const option = { label: "Some text for a label", value: expectedValue }
        const mockOnClick = jest.fn()

        const wrapper = shallowRadioButton(option, false, mockOnClick)
        wrapper.simulate("click")

        expect(mockOnClick).toHaveBeenCalledWith(expectedValue)
    })
})
