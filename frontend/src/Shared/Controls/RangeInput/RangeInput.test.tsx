import { mount, shallow } from "enzyme"
import * as React from "react"
import { RangeInput } from "./RangeInput"

describe("<RangeInput/>", () => {
    it("should render with min and max values", () => {
        const range = { start: 0, end: 10 }

        const wrapper = mount(<RangeInput minValue={0} maxValue={10} setRange={() => {}} range={range} />)
        expect(
            wrapper
                .find(".range-input")
                .at(0)
                .props().value,
        ).toEqual(0)
        expect(
            wrapper
                .find(".range-input")
                .at(1)
                .props().value,
        ).toEqual(10)
    })

    it("should update start value on change", () => {
        const mockSetRange = jest.fn()
        const range = { start: 0, end: 10 }
        const wrapper = shallow(<RangeInput minValue={0} maxValue={10} setRange={mockSetRange} range={range} />)

        const event = {
            currentTarget: {
                value: "4",
            },
        }

        wrapper
            .find(".range-input")
            .at(0)
            .simulate("change", event)

        expect(mockSetRange).toHaveBeenCalledTimes(1)
        expect(mockSetRange).toHaveBeenCalledWith({ start: 4, end: 10 })
    })

    it("should not have class .invalid for valid range ", () => {
        const range = { start: 1, end: 9 }
        const mockSetRange = jest.fn()
        const wrapper = shallow(<RangeInput minValue={0} maxValue={10} setRange={mockSetRange} range={range} />)
        expect(
            wrapper
                .find(".range-input")
                .at(0)
                .hasClass("invalid"),
        ).toBe(false)
        expect(
            wrapper
                .find(".range-input")
                .at(1)
                .hasClass("invalid"),
        ).toBe(false)
    })
})
