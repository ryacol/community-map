import "./CheckButton.css"
import "../Controls.css"

import React from "react"
import { ListButtonOption } from "../ListButtonOption/ListButtonOption"

interface CheckButtonProps<T> {
    isSelected: boolean
    option: ListButtonOption<T>
    onClick: (value: T) => void
    color: string
}

export function CheckButton<T>(props: CheckButtonProps<T>) {
    const { color, option, onClick, isSelected } = props
    const className = `check-box ${isSelected ? `check-box--selected ${color}` : ""}`
    return (
        <div className="check-button" onClick={() => onClick(option.value)}>
            <span className={className}>{isSelected && "✓"}</span>
            <span className="check-button-label">{option.label}</span>
        </div>
    )
}
