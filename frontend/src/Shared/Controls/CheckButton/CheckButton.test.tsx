import React from "react"
import { mount, shallow } from "enzyme"
import { CheckButton } from "./CheckButton"

describe("<CheckButton/>", () => {
    const expectedOption = { label: "UK Office", value: 0 }
    it("should show that its been deselected", () => {
        const wrapper = mount(<CheckButton isSelected={false} option={expectedOption} onClick={() => {}} color="red" />)
        expect(wrapper.hasClass("check-box--selected")).toBe(false)
    })

    it("should show that its been selected", () => {
        const wrapper = mount(<CheckButton isSelected={true} option={expectedOption} onClick={() => {}} color="red" />)
        const checkBox = wrapper.find(".check-box")
        expect(checkBox.hasClass("check-box--selected")).toBe(true)
    })

    it("should display the label from the given option", () => {
        const wrapper = mount(<CheckButton isSelected={false} option={expectedOption} onClick={() => {}} color="red" />)
        const checkButtonLabel = wrapper.find(".check-button-label").text()
        expect(checkButtonLabel).toEqual(expectedOption.label)
    })

    it("should call the setter with the value from the given option when clicked", () => {
        const expectedValue = expectedOption.value
        const mockOnClick = jest.fn()

        const wrapper = shallow(
            <CheckButton isSelected={false} option={expectedOption} onClick={mockOnClick} color="red" />,
        )
        wrapper.simulate("click")

        expect(mockOnClick).toHaveBeenCalledWith(expectedValue)
    })
})
