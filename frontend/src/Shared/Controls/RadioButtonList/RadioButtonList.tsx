import "./RadioButtonList.css"

import React from "react"
import { RadioButton } from "../RadioButton/RadioButton"
import { ListButtonOption } from "../ListButtonOption/ListButtonOption"

interface RadioButtonListProps<T> {
    title: string
    options: ListButtonOption<T>[]
    selectedValue?: T
    setSelectedOption: (value: T) => void
    color: string
}

export function RadioButtonList<T>(props: RadioButtonListProps<T>) {
    return (
        <div className="rb-list">
            <h3 className="rb-list-title">{props.title}</h3>
            {props.options.map((option: ListButtonOption<T>) => (
                <RadioButton
                    key={option.label}
                    option={option}
                    isSelected={props.selectedValue === option.value}
                    onClick={props.setSelectedOption}
                    color={props.color}
                />
            ))}
        </div>
    )
}
