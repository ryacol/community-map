import { mount, shallow } from "enzyme"
import React from "react"
import { RadioButton } from "../RadioButton/RadioButton"
import { RadioButtonList } from "./RadioButtonList"
import { ListButtonOption } from "../ListButtonOption/ListButtonOption"

const defaultOptions: ListButtonOption<number>[] = [
    { label: "foo", value: 0 },
    { label: "bar", value: 1 },
    { label: "fo", value: 2 },
    { label: "fum", value: 3 },
]

function getComponent<T>(
    title = "some-title",
    options: ListButtonOption<T>[],
    selectedValue?: T,
    setSelectedOption: (value: T) => void = jest.fn(),
) {
    return (
        <RadioButtonList
            title={title}
            options={options}
            selectedValue={selectedValue}
            setSelectedOption={setSelectedOption}
            color="teal"
        />
    )
}

describe("<RadioButtonList/>", () => {
    it("should display the given title", () => {
        const expected = "List title"
        const wrapper = shallow(getComponent(expected, defaultOptions))
        const title = wrapper.find(".rb-list-title").text()
        expect(title).toEqual(expected)
    })

    it("should display a radio button for each of its items", () => {
        const wrapper = shallow(getComponent("some-title", defaultOptions))

        const expectedLength = defaultOptions.length
        const radioButtons = wrapper.find(RadioButton)
        expect(radioButtons).toHaveLength(expectedLength)
    })

    it("should display radio buttons with the correct text for all options", () => {
        const wrapper = mount(getComponent("some-title", defaultOptions))
        const radioButtons = wrapper.find(RadioButton)

        radioButtons.forEach((radioButton, i) => {
            const radioButtonText = radioButton.text()
            expect(radioButtonText).toEqual(defaultOptions[i].label)
        })
    })

    it("should not highlight any radio buttons if there is no selected option", () => {
        const wrapper = mount(getComponent("some-title", defaultOptions))
        const radioButtons = wrapper.find(RadioButton)

        radioButtons.forEach(radioButton => {
            const radioButtonIsSelected = radioButton.props().isSelected
            expect(radioButtonIsSelected).toBe(false)
        })
    })

    it("should highlight the selected option", () => {
        const i = 1

        const wrapper = mount(getComponent("some-title", defaultOptions, defaultOptions[i].value))
        const selectedRadioButton = wrapper.find(RadioButton).at(i)

        const radioButtonIsSelected = selectedRadioButton.props().isSelected
        expect(radioButtonIsSelected).toBe(true)
    })

    it("should call the setter on click", () => {
        const mockSetSelectedOption = jest.fn()
        const i = 2
        const wrapper = mount(getComponent("some-title", defaultOptions, undefined, mockSetSelectedOption))

        wrapper
            .find(RadioButton)
            .at(i)
            .simulate("click")

        const expectedOption = defaultOptions[i]
        expect(mockSetSelectedOption).toHaveBeenCalledWith(expectedOption.value)
    })
})
