import "./FilterButton.css"

import React from "react"
import { ListButtonOption } from "../ListButtonOption/ListButtonOption"

interface FilterButtonProps<T> {
    option: ListButtonOption<T>
    onClick: (value: T) => void
    children: any
}

export function FilterButton<T>(props: FilterButtonProps<T>) {
    return (
        <div className="filter-button" onClick={() => props.onClick(props.option.value)}>
            {props.children}
            <div className="filter-label">{props.option.label}</div>
        </div>
    )
}
