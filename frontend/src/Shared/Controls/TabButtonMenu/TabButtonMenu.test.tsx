import React from "react"
import { TabButton } from "../TabButton/TabButton"
import { mount, shallow } from "enzyme"
import { TabButtonMenu } from "./TabButtonMenu"

describe("<TabButtonMenu/>", () => {
    const tabs = [{ label: "Staff", value: "Staff Value" }, { label: "Volunteers", value: "Volunteers Value" }]

    it("should render the right number of tabs", () => {
        const wrapper = shallow(<TabButtonMenu tabs={tabs} onClick={() => {}} selectedTab={tabs[0]} />)
        const renderedTabs = wrapper.find(TabButton)

        expect(renderedTabs).toHaveLength(tabs.length)
    })

    it("should render the correct label for each tab", () => {
        const wrapper = mount(<TabButtonMenu tabs={tabs} onClick={() => {}} selectedTab={tabs[0]} />)
        const renderedTabs = wrapper.find(TabButton)
        renderedTabs.forEach((renderedTab, i) => {
            const renderedTabLabel = renderedTab.find(".tab-button").text()
            expect(renderedTabLabel).toEqual(tabs[i].label)
        })
    })

    it("should select the correct tab", () => {
        const mockOnClick = jest.fn()
        const wrapper = mount(<TabButtonMenu tabs={tabs} onClick={mockOnClick} selectedTab={tabs[0]} />)
        const renderedTabs = wrapper.find(TabButton)
        renderedTabs.forEach((renderedTab, i) => {
            renderedTab.simulate("click")
            expect(mockOnClick).toHaveBeenCalledWith(tabs[i].value)
        })
    })
})
