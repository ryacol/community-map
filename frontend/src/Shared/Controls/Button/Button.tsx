import "./Button.css"
import React from "react"

interface ButtonProps {
    onClick: () => void
    label: string
}

export const Button = (props: ButtonProps) => {
    const { label, onClick } = props

    return (
        <button onClick={onClick} className="button">
            {label}
        </button>
    )
}
