import "./CollapseButton.css"

import CollapseIcon from "./CollapseIcon.svg"
import React from "react"

interface CollapseButtonProps {
    isCollapsed: boolean
    toggleCollapsed: () => void
}

export function CollapseButton(props: CollapseButtonProps) {
    const { isCollapsed, toggleCollapsed } = props
    const collapseButtonClass: string = isCollapsed ? "collapse-button--closed" : ""
    return (
        <div id="pane-collapse" onClick={toggleCollapsed}>
            <div id="pane-shadow" />
            <img
                src={CollapseIcon}
                id="collapse-button"
                className={collapseButtonClass}
                alt="collapse icon"
                aria-hidden="true"
            />
        </div>
    )
}
