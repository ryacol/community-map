const baseUrl = `${window.location.origin}/data`

export const fetchData = async (file: string): Promise<any> => {
    const response = await fetch(`${baseUrl}/${file}`)
    return response.json()
}

export const getTilesUrl = (fileIdentifier: string): string => {
    return `${baseUrl}/${fileIdentifier}/{z}/{x}/{y}.pbf`
}
