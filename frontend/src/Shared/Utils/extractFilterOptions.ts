import { FilterOptions } from "../FilterOptions/FilterOptions"

export const extractSelectedOptions = (filterOptions: FilterOptions) => {
    if (!filterOptions) return []
    return Object.entries(filterOptions)
        .filter(([_, isSelected]) => isSelected === true)
        .map(([name, _]) => name)
}
