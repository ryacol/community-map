import { fetchData } from "./fetchData"

const localBaseUrl = "http://localhost/data"
const mockFetch = jest.fn().mockImplementation(() =>
    Promise.resolve({
        json: () => {},
    }),
)

describe("fetchData", () => {
    it("should fetch data for a given file name", () => {
        window.fetch = mockFetch
        const fileName = "foo.json"
        fetchData(fileName)
        expect(mockFetch).toHaveBeenCalledWith(`${localBaseUrl}/${fileName}`)
    })
})
