import React, { createContext, useState } from "react"
import config, { SurveyData } from "../Core/Config/config"
import { extractSelectedOptions } from "../Shared/Utils/extractFilterOptions"
import { fetchData } from "../Shared/Utils/fetchData"

export const SurveyContext = createContext<any>({})

type surveyConfig = Record<string, SurveyData>

const metadata: surveyConfig = config.data.surveys
    .map(survey => ({ [survey.title]: survey }))
    .reduce((a: surveyConfig, b: surveyConfig) => ({ ...a, ...b }))

export function SurveyContextWrapper(props: any) {
    const [survey, setSurvey] = useState<any>({
        dataType: undefined,
        domain: undefined,
        range: undefined,
        riskDomains: undefined,
        sourceLayer: "",
        fileIdentifier: "",
        maxRange: 10,
        filters: [],
        sources: [],
        selectedSource: undefined,
    })

    const _setSurveyProperty = (property: string) => (value: any) => {
        setSurvey({ ...survey, [property]: value })
    }

    const _fetchAndUpdateSurveyData = async (source: string, dataType?: string) => {
        const data = await fetchData(source)
        let newSurvey = {
            ...survey,
            sourceLayer: data.tilestats.layers[0].layer,
            fileIdentifier: source.substr(0, source.indexOf(".")),
            selectedSource: source,
        }

        if (dataType) {
            const domain = metadata[dataType].filters[0].value
            const range = data.tilestats.layers[0].attributes.find((record: any) => record.attribute === domain)
            newSurvey = {
                ...newSurvey,
                dataType,
                domain,
                maxRange: range.max,
                range: { start: 1, end: range.max },
                riskDomains: { [metadata["Risk"].filters[0].value]: true },
                filters: metadata[dataType].filters,
                sources: metadata[dataType].sources,
            }
        }

        setSurvey({
            ...newSurvey,
        })
    }

    const setSurveySource = async (source: string) => {
        _fetchAndUpdateSurveyData(source)
    }

    const setSurveyDataType = async (dataType: string) => {
        if (dataType) {
            // Default to the first source file (for most domains, there is only one source anyway)
            const source = metadata[dataType].sources[0].value
            _fetchAndUpdateSurveyData(source, dataType)
        } else {
            setSurvey(undefined)
        }
    }

    const toggleRiskDomains = (risk: string) => {
        const risksToSet = { ...survey.riskDomains, [risk]: !survey.riskDomains[risk] }
        if (extractSelectedOptions(risksToSet).length > 0) _setSurveyProperty("riskDomains")(risksToSet)
    }

    const value = {
        ...survey,
        setRange: _setSurveyProperty("range"),
        setSurveyDataTypeInternal: _setSurveyProperty("dataType"),
        setSurveyDomain: _setSurveyProperty("domain"),
        setRiskDomains: _setSurveyProperty("riskDomains"),
        toggleRiskDomains,
        setSurveyDataType,
        setSurveySource,
        survey,
        setSurvey,
    }

    return <SurveyContext.Provider value={value}>{props.children}</SurveyContext.Provider>
}
