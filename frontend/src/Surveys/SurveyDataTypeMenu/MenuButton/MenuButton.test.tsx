import { shallow } from "enzyme"
import { MenuButton } from "./MenuButton"
import React from "react"

const shallowMenuButton = (text = "", isSelected = false, onClick = jest.fn()) =>
    shallow(<MenuButton text={text} isSelected={isSelected} onClick={onClick} buttonId="some-id" />)

describe("<MenuButton/>", () => {
    it("should display the survey datatype to select", () => {
        const wrapper = shallowMenuButton("Deprivation")

        const buttonText = wrapper.text()
        expect(buttonText).toEqual("Deprivation")
    })

    describe("isSelected", () => {
        it("should not be highlighted if the button is not selected", () => {
            const wrapper = shallowMenuButton("some-text", false)
            expect(wrapper.find(".menu-button").hasClass("selected")).toBe(false)
        })

        it("should be highlighted if the button is selected", () => {
            const wrapper = shallowMenuButton("some-text", true)
            expect(wrapper.find(".menu-button").hasClass("selected")).toBe(true)
        })
    })

    describe("onClick", () => {
        it("should call its setter", () => {
            const expectedValue = "Bar"
            const mockSetter = jest.fn()
            const wrapper = shallowMenuButton(expectedValue, false, mockSetter)

            wrapper.simulate("click")

            expect(mockSetter).toHaveBeenCalledWith(expectedValue)
        })
    })
})
