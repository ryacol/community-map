import "./SurveyDataTypeMenu.css"
import config from "../../Core/Config/config"

import React, { useContext } from "react"
import { MenuButton } from "./MenuButton/MenuButton"
import { SurveyContext } from "../SurveyContext"

export function SurveyDataTypeMenu() {
    const { dataType: surveyDataType, setSurveyDataType } = useContext(SurveyContext)

    const toggleSurveyDataType = (selectedSurveyOption: string) => {
        const newSurveyDataType = surveyDataType === selectedSurveyOption ? undefined : selectedSurveyOption
        setSurveyDataType(newSurveyDataType)
    }

    return (
        <div className="survey-datatype-menu">
            {config.data.surveys.map(data => (
                <MenuButton
                    key={data.title}
                    text={data.title}
                    isSelected={data.title === surveyDataType}
                    onClick={() => toggleSurveyDataType(data.title)}
                    buttonId={`button_${data.title.toLowerCase()}`}
                />
            ))}
        </div>
    )
}
