import "./QuantileLegend.css"

import { LegendValue } from "./LegendValue"
import React from "react"
import { LegendItem } from "./LegendItem"

interface LegendProps {
    legendItems: LegendValue[]
}

export function QuantileLegend({ legendItems }: LegendProps) {
    return (
        <div className="legend">
            {legendItems.map((legendItem: LegendValue) => (
                <LegendItem key={legendItem.value} legendItem={legendItem} />
            ))}
        </div>
    )
}
