import "./SurveyDomainMenu.css"

import { getLegendValuesFor } from "./QuantileLegend/LegendValue"
import React, { useContext } from "react"
import { DomainSettingsHeader } from "./DomainSettingsHeader/DomainSettingsHeader"
import { DomainSettingsBody } from "./DomainSettingsBody/DomainSettingsBody"
// We didn't want it to come to this, but we tried the suggested fixes and they didn't work so we had to do the ts-ignore.
// The issue is because this library hasn't defined types for TypeScript
// eslint-disable-next-line @typescript-eslint/ban-ts-ignore
// @ts-ignore
import useCollapse from "react-collapsed"
import { QuantileLegend } from "./QuantileLegend/QuantileLegend"
import { SurveyContext } from "../SurveyContext"

export function SurveyDomainMenu() {
    const { getCollapseProps, getToggleProps, isOpen } = useCollapse({
        defaultOpen: true,
    })

    const { dataType: surveyDataType, maxRange } = useContext(SurveyContext)

    if (surveyDataType) {
        return (
            <div id="survey-domain-menu">
                <DomainSettingsHeader isOpen={isOpen} getToggleProps={getToggleProps} surveyDataType={surveyDataType} />
                <QuantileLegend legendItems={getLegendValuesFor(maxRange)} />
                <DomainSettingsBody getCollapseProps={getCollapseProps} />
            </div>
        )
    } else {
        return null
    }
}
