import "./DomainSettingsHeader.css"

import React from "react"
import { ChevronButton } from "../ChevronButton/ChevronButton"

interface DomainSettingsHeaderProps {
    surveyDataType: string
    isOpen: boolean
    getToggleProps: any
}

export function DomainSettingsHeader(props: DomainSettingsHeaderProps) {
    const { getToggleProps, isOpen, surveyDataType } = props

    return (
        <div id="domain-settings--header" className="header-with-collapsible-body">
            <h2 id="domain-settings--title" className="header-title">
                {surveyDataType} index
            </h2>
            <ChevronButton {...getToggleProps()} isOpen={isOpen} />
        </div>
    )
}
