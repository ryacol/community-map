import { shallow } from "enzyme"
import React from "react"
import { DomainSettingsHeader } from "./DomainSettingsHeader"

describe("<DomainSettingsHeader/>", () => {
    it("should display the selected survey datatype in the title", () => {
        const wrapper = shallow(
            <DomainSettingsHeader surveyDataType={"Deprivation"} getToggleProps={jest.fn} isOpen={true} />,
        )

        const titleText = wrapper.find("#domain-settings--title").text()

        const expected = "Deprivation index"
        expect(titleText).toEqual(expected)
    })
})
