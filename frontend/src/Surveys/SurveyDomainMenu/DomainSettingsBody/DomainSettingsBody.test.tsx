import { mountWithContext } from "../../../Shared/Utils/testUtils"
import { DomainSettingsBody } from "./DomainSettingsBody"
import React from "react"
import { RadioButtonList } from "../../../Shared/Controls/RadioButtonList/RadioButtonList"
import { SurveyContext } from "../../SurveyContext"

const mountComponent = (sources: Record<string, string>[]) => {
    return mountWithContext(
        <SurveyContext.Provider value={{ riskDomains: [], filters: [], sources }}>
            <DomainSettingsBody getCollapseProps={() => {}} />
        </SurveyContext.Provider>,
    )
}

describe("<DomainSettingsBody>", function() {
    it("should not display data source selection if only one source is available for survey", () => {
        const sources = [
            {
                label: "foo",
                value: "foo.json",
            },
        ]
        const wrapper = mountComponent(sources)
        expect(wrapper.find(RadioButtonList)).toHaveLength(1)
    })

    it("should display data source selection if more than one source is available for survey", () => {
        const sources = [
            {
                label: "foo",
                value: "foo.json",
            },
            {
                label: "bar",
                value: "bar.json",
            },
        ]
        const wrapper = mountComponent(sources)
        expect(wrapper.find(RadioButtonList)).toHaveLength(2)
        expect(
            wrapper
                .find(RadioButtonList)
                .first()
                .find("h3")
                .text(),
        ).toEqual("Granularity")
    })
})
