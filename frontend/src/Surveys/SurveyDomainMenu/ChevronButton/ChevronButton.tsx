import "./ChevronButton.css"

import React from "react"
import ChevronIcon from "./ChevronIcon.svg"

interface ChevronButtonProps {
    isOpen: boolean
    onClick: () => void
}

export const ChevronButton = ({ isOpen, onClick }: ChevronButtonProps) => {
    let chevronButtonClassName = "chevron-button"
    if (!isOpen) {
        chevronButtonClassName += " chevron-button--closed"
    }
    return (
        <div className={chevronButtonClassName} onClick={onClick}>
            <img src={ChevronIcon} className="chevron" alt="chevron icon" aria-hidden="true" />
        </div>
    )
}
