import React, { useContext } from "react"
import { BoundaryContext } from "./BoundaryContext"
import { mount } from "enzyme"

jest.mock("../Core/Config/config", () => ({
    default: {
        data: {
            boundaries: [
                {
                    label: "a",
                    fileIdentifier: "b",
                },
            ],
        },
    },
}))

jest.mock("../Shared/Utils/fetchData", () => ({
    fetchData: (value: string) => "c",
}))

describe("Boundary Context", function() {
    it("should provide it children with the Boundary Context", function() {
        const expectedValue = {
            label: "a",
            fileIdentifier: "b",
            source: "c",
        }

        const test = () => {
            const { boundary } = useContext(BoundaryContext)
            expect(boundary).toEqual(expectedValue)
            return <div />
        }

        mount(
            <BoundaryContext.Provider value={{ boundary: expectedValue }}>
                {React.createElement(test)}
            </BoundaryContext.Provider>,
        )
    })
})
