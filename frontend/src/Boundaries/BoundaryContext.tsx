import React, { useState } from "react"
import config, { BoundaryData } from "../Core/Config/config"
import { fetchData } from "../Shared/Utils/fetchData"

export const BoundaryContext = React.createContext<any>({})

export type BoundaryWithSource = BoundaryData & { source: string }

export function BoundaryContextWrapper(props: any) {
    const [boundary, _setBoundary] = useState<BoundaryWithSource>({
        label: "",
        fileIdentifier: "",
        source: "",
    })

    const setBoundary = async (newBoundary: string) => {
        if (boundary.label === newBoundary)
            _setBoundary({
                label: "",
                fileIdentifier: "",
                source: "",
            })
        else {
            const boundaryData = config.data.boundaries.find((boundary: any) => boundary.label === newBoundary)
            if (!!boundaryData && boundaryData.fileIdentifier) {
                const data = await fetchData(`${boundaryData.fileIdentifier}.json`)
                _setBoundary({
                    label: newBoundary,
                    fileIdentifier: boundaryData.fileIdentifier,
                    source: data.tilestats.layers[0].layer,
                })
            }
        }
    }

    const value = { boundary, _setBoundary, setBoundary }

    return <BoundaryContext.Provider value={value}>{props.children}</BoundaryContext.Provider>
}
