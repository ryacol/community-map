import "./App.css"

import React from "react"
import MapboxMap from "../../Map/MapboxMap/MapboxMap"
import { SurveyDataTypeMenu } from "../../Surveys/SurveyDataTypeMenu/SurveyDataTypeMenu"
import { Pane } from "../../Shared/Pane/Pane"
import { SurveyDomainMenu } from "../../Surveys/SurveyDomainMenu/SurveyDomainMenu"
import { MapboxContextWrapper } from "../../Map/MapboxContext"
import { ShareMenu } from "../../Sharing/ShareMenu/ShareMenu"
import { URLListener } from "../../Sharing/URLListener/URLListener"
import { SurveyContextWrapper } from "../../Surveys/SurveyContext"
import { Snackbar } from "../../Shared/Snackbar/Snackbar"
import { SnackbarContextWrapper } from "../../Shared/Snackbar/SnackbarContext"
import { ResourceContextWrapper } from "../../Resources/ResourceContext"
import { BoundaryContextWrapper } from "../../Boundaries/BoundaryContext"
import { BoundaryMenu } from "../../Boundaries/BoundaryMenu/BoundaryMenu"
import config from "../Config/config"

function App() {
    return (
        <div id="app-container">
            {// This section of code loads the pins used by the resource layer
            // into the static media folder, as required by MapBox for media used
            // after the map has already been rendered
            config.data.resources
                .map(resource => resource.tabs)
                .reduce((a, b) => [...a, ...b])
                .map(tab => (
                    <img
                        alt={tab.title}
                        key={tab.title}
                        src={require("../Config/Pins/" + tab.icon)}
                        style={{ display: "none" }}
                    />
                ))}
            <ResourceContextWrapper>
                <SurveyContextWrapper>
                    <SnackbarContextWrapper>
                        <BoundaryContextWrapper>
                            <MapboxContextWrapper>
                                <div id="overlay-container">
                                    <Pane />
                                    <div id="menu-container">
                                        <div id="bar-container">
                                            <SurveyDataTypeMenu />
                                        </div>
                                        <div id="domain-menu-container">
                                            <SurveyDomainMenu />
                                        </div>
                                    </div>

                                    <BoundaryMenu />
                                    <ShareMenu />
                                </div>
                                <div id="view-container">
                                    <div id="map-container">
                                        <MapboxMap />
                                        <URLListener />
                                    </div>
                                    <Snackbar />
                                </div>
                            </MapboxContextWrapper>
                        </BoundaryContextWrapper>
                    </SnackbarContextWrapper>
                </SurveyContextWrapper>
            </ResourceContextWrapper>
        </div>
    )
}

export default App
