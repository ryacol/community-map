import config from "./config.json"

export interface SurveyFilter {
    label: string
    value: string
}

export interface SurveyData {
    sources: {
        label: string
        value: string
    }[]
    title: string
    filters: SurveyFilter[]
}

export interface ResourceFilter {
    label: string
    attribute: string
}

export interface ResourceTab {
    JSON: string
    title: string
    icon: string
    filters: ResourceFilter[]
}

export interface ResourceData {
    menuTitle: string
    menuIcon: string
    tabs: ResourceTab[]
}

export interface BoundaryData {
    label: string
    fileIdentifier: string
}

export interface Config {
    data: {
        surveys: SurveyData[]
        resources: ResourceData[]
        boundaries: BoundaryData[]
    }
}

export default config as Config
