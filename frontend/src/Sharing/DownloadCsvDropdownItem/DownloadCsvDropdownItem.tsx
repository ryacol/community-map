import React, { useContext } from "react"
import { MapboxContext } from "../../Map/MapboxContext"
import { DropdownItem } from "../../Shared/Controls/DropdownItem/DropdownItem"
import { downloadFromDataUrl } from "../../Shared/Utils/downloadFromDataUrl"
import { SnackbarContext } from "../../Shared/Snackbar/SnackbarContext"
import { SurveyContext } from "../../Surveys/SurveyContext"
import { ResourceContext } from "../../Resources/ResourceContext"
import config from "../../Core/Config/config"

const resourceTypes = config.data.resources
    .map(menu => menu.tabs)
    .reduce((a, b) => [...a, ...b])
    .map(tab => tab.title)

export const DownloadCsvDropdownItem = () => {
    const { dataType: surveyDataType, fileIdentifier } = useContext(SurveyContext)
    const { resources } = useContext(ResourceContext)
    const { map } = useContext(MapboxContext)
    const { displayMessage } = useContext(SnackbarContext)

    const dataIsAvailableForDownload = () => {
        return (
            surveyDataType !== undefined ||
            resourceTypes
                .map(
                    resource =>
                        resource in resources &&
                        map.queryRenderedFeatures(null, { layers: [`layer_${resource}`] }).length > 0,
                )
                .reduce((a, b) => a || b)
        )
    }

    const exportLayerToCsv = (label: string) => {
        const data = map
            .queryRenderedFeatures(null, { layers: [`layer_${label}`] })
            .map((regionData: any) => regionData.properties)

        if (data && data.length > 0) {
            const csv = convertToCSV(data)
            downloadCsv(csv, label)
        }
    }

    const exportResourcesData = () => {
        resourceTypes.forEach(resource => {
            if (resources[resource]) exportLayerToCsv(resources[resource].dataLabel)
        })
    }

    const exportSurveyData = () => {
        if (surveyDataType) {
            exportLayerToCsv(fileIdentifier)
        }
    }

    const exportCsvs = () => {
        if (dataIsAvailableForDownload()) {
            exportSurveyData()
            exportResourcesData()
        } else {
            // Note: This can't simply be done before the user clicks due to async map data loading.
            displayMessage("No data is displayed")
        }
    }

    return <DropdownItem onClick={exportCsvs} label={"Download CSVs"} disabled={!map.queryRenderedFeatures} />
}

// https://stackoverflow.com/questions/8847766/how-to-convert-json-to-csv-format-and-store-in-a-variable
export function convertToCSV(input: Record<string, any>[]) {
    if (!input || input.length === 0) {
        console.debug("Empty input for CSV conversion")
        return ""
    }

    const replacer = (key: string, value: any) => (value === null ? "" : value)
    const header = Object.keys(input[0])
    const csv = input.map(row => header.map(fieldName => JSON.stringify(row[fieldName], replacer)).join(","))
    csv.unshift(header.join(","))
    return csv.join("\r\n")
}

const downloadCsv = (csv: string, fileName: string) => {
    const dataUrl = convertBlobToDataUrl(csv, fileName, "text/csv;charset=utf-8;")
    return downloadFromDataUrl(dataUrl, fileName)
}

const convertBlobToDataUrl = (payload: string, fileName: string, type: string) => {
    const blob = new Blob([payload], { type })
    return URL.createObjectURL(blob)
}
