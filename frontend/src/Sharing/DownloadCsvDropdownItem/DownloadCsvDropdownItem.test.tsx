/* eslint @typescript-eslint/camelcase: 0 */
import { convertToCSV } from "./DownloadCsvDropdownItem"

describe("<DownloadCsvDropdownItem>", () => {
    it("should transform JSON to CSV", () => {
        const input = [
            {
                Health_Rank: "16589",
                Income_Rank: "13894",
                Environment_Rank: "17425",
                Name: "East Riding of Yorkshire 031B",
                IMD_Decile: 5,
                Employment_Rank: "13449",
            },
            {
                Health_Rank: "16589",
                Income_Rank: "13894",
                Environment_Rank: "17425",
                Name: "East Riding of Yorkshire 031B",
                IMD_Decile: 5,
                Employment_Rank: "13449",
            },
        ]

        const expected =
            "Health_Rank,Income_Rank,Environment_Rank,Name,IMD_Decile,Employment_Rank\r\n" +
            '"16589","13894","17425","East Riding of Yorkshire 031B",5,"13449"\r\n' +
            '"16589","13894","17425","East Riding of Yorkshire 031B",5,"13449"'

        expect(convertToCSV(input)).toEqual(expected)
    })
})
