import "./ShareMenu.css"
import React from "react"
import { DropdownMenu } from "../DropdownMenu/DropdownMenu"
import { ShareDropdownItem } from "../ShareDropdownItem/ShareDropdownItem"
import { DownloadCsvDropdownItem } from "../DownloadCsvDropdownItem/DownloadCsvDropdownItem"
import { DownloadPngDropdownItem } from "../DownloadPngDropdownItem/DownloadPngDropdownItem"
import { DownloadPdfDropdownItem } from "../DownloadPdfDropdownItem/DownloadPdfDropdownItem"

export const ShareMenu = () => {
    return (
        <div id="share-menu">
            <DropdownMenu label={"Share"}>
                <ShareDropdownItem />
                <DownloadCsvDropdownItem />
                <DownloadPngDropdownItem />
                <DownloadPdfDropdownItem />
            </DropdownMenu>
        </div>
    )
}
