import React, { useContext } from "react"
import { MapboxContext } from "../../Map/MapboxContext"
import { DropdownItem } from "../../Shared/Controls/DropdownItem/DropdownItem"
import { downloadFromDataUrl } from "../../Shared/Utils/downloadFromDataUrl"

export const DownloadPngDropdownItem = () => {
    const { map } = useContext(MapboxContext)

    const handleClick = () => {
        downloadPng(map.getCanvas(), "map.png")
    }

    return <DropdownItem onClick={handleClick} label={"Download PNG"} disabled={map.getCanvas === undefined} />
}

const downloadPng = (canvas: HTMLCanvasElement, fileName: string) => {
    const dataUrl = canvas.toDataURL("image/png")
    downloadFromDataUrl(dataUrl, fileName)
}
