import { mountWithContext } from "../../Shared/Utils/testUtils"
import React, { ReactElement } from "react"
import { DownloadPdfDropdownItem } from "./DownloadPdfDropdownItem"
import { DropdownItem } from "../../Shared/Controls/DropdownItem/DropdownItem"
import { MapboxContext } from "../../Map/MapboxContext"

const mockJsPdf = {
    addImage: jest.fn(),
    save: jest.fn(),
    setProperties: jest.fn(),
}
jest.mock("jspdf", () => jest.fn(() => mockJsPdf))

function mountWithContextAndMockMap(element: ReactElement) {
    const MockMap = {
        getCanvas: () => ({
            width: 1,
            height: 1,
            toDataURL: () => "",
        }),
    }
    return mountWithContext(<MapboxContext.Provider value={{ map: MockMap }}>{element}</MapboxContext.Provider>)
}

describe("<DownloadPdfDropdownItem>", function() {
    it("should be disabled while the Map component is not ready", function() {
        const wrapper = mountWithContext(<DownloadPdfDropdownItem />)
        expect(wrapper.find(DropdownItem).props().disabled).toEqual(true)
    })

    it("should be enabled once the Map component is ready", function() {
        const wrapper = mountWithContextAndMockMap(<DownloadPdfDropdownItem />)
        expect(wrapper.find(DropdownItem).props().disabled).toEqual(false)
    })

    it("should download PDF when being clicked on", () => {
        const wrapper = mountWithContextAndMockMap(<DownloadPdfDropdownItem />)

        wrapper.find("button").simulate("click")

        expect(mockJsPdf.addImage).toHaveBeenCalledTimes(1)
        expect(mockJsPdf.setProperties).toHaveBeenCalledTimes(1)
        expect(mockJsPdf.save).toHaveBeenCalledTimes(1)
    })
})
