import React, { useContext, useEffect, useState } from "react"
import { MapboxContext } from "../../Map/MapboxContext"
import { SurveyContext } from "../../Surveys/SurveyContext"
import { ResourceContext } from "../../Resources/ResourceContext"
import { BoundaryContext } from "../../Boundaries/BoundaryContext"

export function URLListener() {
    const [mapOptions, setMapOptions] = useState<Record<string, any>>()
    const { map } = useContext(MapboxContext)
    const { setSurvey } = useContext(SurveyContext)
    const { setResources } = useContext(ResourceContext)
    const { _setBoundary } = useContext(BoundaryContext)

    const setters: any = {
        survey: setSurvey,
        resources: setResources,
        boundary: _setBoundary,
        map: (opt: any) => setMapOptions(opt),
    }

    useEffect(() => {
        const parseLink = () => {
            try {
                const stateFromURL = JSON.parse(atob(window.location.search.substring(1)))
                Object.keys(stateFromURL).forEach((key: string) => setters[key](stateFromURL[key]))
                window.history.pushState({}, "", "/")
            } catch (e) {
                window.history.pushState({}, "", "/")
            }
        }
        parseLink()
        // Disable react-hooks/exhaustive-deps rule
        // eslint-disable-next-line
    }, [])

    useEffect(() => {
        if (map.flyTo && map.getZoom && map.getCenter) {
            map.flyTo(mapOptions)
        }
        // Disable react-hooks/exhaustive-deps rule
        // eslint-disable-next-line
    }, [map.flyTo])

    return <React.Fragment />
}
