import "./ResourceMenu.css"

import React, { useContext } from "react"
import config from "../../Core/Config/config"
import { FilterButton } from "../../Shared/Controls/FilterButton/FilterButton"
import { ClearButton } from "../../Shared/Controls/ClearButton/ClearButton"
import { ResourceContext } from "../ResourceContext"

interface ResourcesMenuProps {
    setMenu: (menu?: string) => void
}

export const Badge = (props: { value: number }) =>
    props.value === 0 ? null : <span className={"badge"}>{props.value}</span>

export function ResourcesMenu(props: ResourcesMenuProps) {
    const { getFilterCount, clearFilter } = useContext(ResourceContext)

    const getFilterCountFor = (menu: any) => {
        return menu.tabs.map((tab: any) => getFilterCount(tab.title)).reduce((a: number, b: number) => a + b)
    }

    const clearFiltersFor = (menu: any) => {
        return menu.tabs.forEach((tab: any) => {
            clearFilter(tab.title)
        })
    }

    return (
        <React.Fragment>
            <h2>Red Cross data filters</h2>
            <div className="resource-selection-pane">
                {config.data.resources.map(resource => (
                    <FilterButton
                        option={{ label: resource["menuTitle"], value: resource["menuTitle"] }}
                        onClick={() => props.setMenu(resource["menuTitle"])}
                        key={resource["menuIcon"]}
                    >
                        <Badge value={getFilterCountFor(resource)} />
                        <img
                            alt={resource["menuTitle"]}
                            src={require("../../Core/Config/Icons/" + resource["menuIcon"])}
                            className="svg-filter-button"
                        />
                    </FilterButton>
                ))}
            </div>
            <hr />
            <ClearButton
                clearFunctions={config.data.resources.map(resource => () => clearFiltersFor(resource))}
                filterCounts={config.data.resources.map(resource => getFilterCountFor(resource))}
            />
        </React.Fragment>
    )
}
