import React, { createContext, useState } from "react"
import config from "../Core/Config/config"
import { fetchData } from "../Shared/Utils/fetchData"
import { extractSelectedOptions } from "../Shared/Utils/extractFilterOptions"

export const ResourceContext = createContext<any>({})

export function ResourceContextWrapper(props: any) {
    const [resources, setResources] = useState<any>({
        menu: undefined,
        tab: undefined,
        menuData: undefined,
        tabData: undefined,
    })

    const convertObj = (array: any[]) => {
        if (array && array.length > 0) {
            return array.reduce((a: any, b: any) => ({ ...a, ...b }))
        }
    }

    const getOptions = async (tab: string, tabData: any) => {
        if (Object.keys(resources).includes(tab)) return resources[tab]
        const dataWanted = tabData.filters.map((data: any) => data.value)
        const data = await fetchData(tabData.JSON)
        const options = convertObj(
            data.tilestats.layers[0].attributes
                .filter((attr: any) => dataWanted.includes(attr.attribute))
                .map((attr: any) => ({
                    [attr.attribute]: convertObj(attr.values.map((value: string) => ({ [value]: false }))),
                })),
        )
        return {
            ...options,
            dataLabel: data.tilestats.layers[0].layer,
            icon: tabData.icon,
            dataWanted,
        }
    }

    const setMenu = async (menu: string | undefined) => {
        if (menu) {
            const menuData: any = config.data.resources.find(data => data["menuTitle"] === menu)
            const tabData = menuData.tabs[0]
            const tab = tabData.title
            const options = await getOptions(tab, tabData)
            setResources({ ...resources, menuData, tabData, menu, tab, [tab]: options })
        } else {
            setResources({ ...resources, menu })
        }
    }

    const setTab = async (tab: string) => {
        const tabData = resources.menuData.tabs.find((data: any) => data.title === tab)
        const options = await getOptions(tab, tabData)
        setResources({ ...resources, tabData, tab, [tab]: options })
    }

    const getFilterCount = (filter: string) => {
        if (!resources[filter]) return 0
        return Object.values(resources[filter])
            .map((x: any) => extractSelectedOptions(x).length)
            .reduce((a, b) => a + b)
    }

    const setFilter = (filterLabel: any) => (filterOption: any) => {
        setResources({
            ...resources,
            [resources.tab]: {
                ...resources[resources.tab],
                [filterLabel]: {
                    ...resources[resources.tab][filterLabel],
                    [filterOption]: !resources[resources.tab][filterLabel][filterOption],
                },
            },
        })
    }

    const resetCheckList = (checkList: any) => {
        Object.keys(checkList).forEach(key => {
            checkList[key] = false
        })
        return checkList
    }

    const clearFilter = (filter: string) => {
        if (!!resources[filter]) {
            setResources({
                ...resources,
                [filter]: {
                    ...resources[filter],
                    ...resources[filter].dataWanted.map((key: string) => ({
                        [key]: resetCheckList(resources[filter][key]),
                    })),
                },
            })
        }
    }

    const value = {
        ...resources,
        resources,
        setResources,
        setMenu,
        setTab,
        setFilter,
        getFilterCount,
        clearFilter,
    }

    return <ResourceContext.Provider value={value}>{props.children}</ResourceContext.Provider>
}
