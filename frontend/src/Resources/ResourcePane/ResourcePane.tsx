import "./ResourcePane.css"

import React, { useContext } from "react"
import { ResourcesMenu } from "../ResourcesMenu/ResourcesMenu"
import { ResourceFilterMenu } from "../ResourceFilterMenu/ResourceFilterMenu"
import { ResourceContext } from "../ResourceContext"

export function ResourcePane() {
    const { menu, setMenu } = useContext(ResourceContext)
    return (
        <div className="filter-settings-container">
            {menu ? (
                <ResourceFilterMenu menu={menu} goBack={() => setMenu(undefined)} />
            ) : (
                <ResourcesMenu setMenu={setMenu} />
            )}
        </div>
    )
}
