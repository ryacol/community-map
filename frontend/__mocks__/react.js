import React from "react"

// To make useEffect deprivation called in tests
module.exports = { ...React, useEffect: React.useLayoutEffect }
