import json
import unittest

from conversion.convert_json_to_geojson import convert_json_to_geojson


class TestCovertJsonToGeoJson(unittest.TestCase):
    def test_converts_json_to_geojson(self):
        test_dir_path = '../test_data/'
        input_path = test_dir_path + 'test_input.json'
        output_path = test_dir_path + 'actual.geojson'
        expected_path = test_dir_path + 'expected.geojson'

        with open(expected_path, 'r') as f:
            expected = json.load(f)

        convert_json_to_geojson(input_path, output_path)

        with open(output_path, 'r') as f:
            actual = json.load(f)

        self.assertEqual(actual, expected)


if __name__ == '__main__':
    unittest.main()
