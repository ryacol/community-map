"""
Converts JSON to GeoJSON format for point coordinates.
"""

import json

property_keys = [
    'id',
    'Basis',
    'Division',
    'Directorate'
]

geometry_keys = ['Longitude', 'Latitude']


def convert_json_to_geojson(json_input_path: str, geojson_output_path: str):
    """
    Reads a JSON file with a particular scheme, converts it to GeoJSON format
    and writes it to disk.
    :param json_input_path: path to the JSON input
    :param geojson_output_path: path to the GeoJSON output
    """
    with open(json_input_path, 'r') as f:
        input_data = json.load(f)

    output_data = {'type': 'FeatureCollection', 'features': []}

    for input_item in input_data:
        output_item = {
            'type': 'Feature',
            'geometry': {
                'type': 'Point',
                'coordinates': [0, 0]
            },
            'properties': {}
        }

        for key in property_keys:
            if key in input_item:
                output_item['properties'][key] = input_item[key]

        for key in geometry_keys:
            if key in input_item and key == 'Longitude':
                output_item['geometry']['coordinates'][0] = input_item[key]
            if key in input_item and key == 'Latitude':
                output_item['geometry']['coordinates'][1] = input_item[key]

        output_data['features'].append(output_item)

    with open(geojson_output_path, 'w') as f:
        json.dump(output_data, f)
