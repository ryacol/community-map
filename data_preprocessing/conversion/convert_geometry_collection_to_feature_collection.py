"""
Converts GeometryCollection JSON to FeatureCollection GeoJSON.
"""

import json

def convert_geometry_collection_to_feature_collection(json_input_path: str, geojson_output_path: str):
    """
    Reads a JSON file with a geometry collection, converts it to GeoJSON format of a feature collection
    and writes it to disk.
    :param json_input_path: path to the JSON input
    :param geojson_output_path: path to the GeoJSON output
    """
    with open(json_input_path, 'r') as f:
        input_data = json.load(f)

    output_data = {'type': 'FeatureCollection', 'features': []}
    id = 0

    for geometry in input_data['geometries']:
        id += 1
        output_item = {
            'type': 'Feature',
            'geometry': geometry,
            'properties': {'id': id}
        }

        output_data['features'].append(output_item)

    with open(geojson_output_path, 'w') as f:
        json.dump(output_data, f)
