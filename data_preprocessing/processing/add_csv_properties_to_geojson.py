import json


def add_csv_properties_to_geojson(csv_properties: dict, input_file: str, output_file: str, lsoa_name: str):
    """
    Reads the relevant GeoJSON file, processes relevant properties.
    :param csv_properties: property to row index mapping
    """
    with open(input_file, 'r') as f:
        data = json.load(f)

    for feat in data['features']:
        properties_dict = csv_properties[feat['properties'][lsoa_name]]
        properties_iter = zip(properties_dict.keys(), properties_dict.values())

        for prop in properties_iter:
            key, value = prop
            feat['properties'][key] = value

    with open(output_file, 'w') as f:
        json.dump(data, f)
