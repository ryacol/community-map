"""
Converts the data representing demographics from
the initial CSV file to GeoJSON format.
"""
import csv
import json
import math

column_indices = {
    "LSOA_Code": 0,
    "no_English": 1,
    "n_older_limited": 3,
    "n_all_limited": 5,
    "prop_alone": 6,
    "n_older_alone": 10
}

max_values = {
    "no_English": 154,
    "n_older_limited": 334,
    "n_all_limited": 593,
    "prop_alone": 0.8732,
    "n_older_alone": 527
}


def build_demographics_geojson_from_csv(coordinates_map: dict):
    demographics = {'type': 'FeatureCollection', 'features': []}

    with open('../data/UK_demographics_LSOA.csv') as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        next(csv_reader)
        id = 1

        for row in csv_reader:
            new_feature = {'type': 'Feature', 'id': id}

            lsoa_code = row[column_indices["LSOA_Code"]]
            n_english = int(math.ceil(float(row[column_indices["no_English"]]) / max_values["no_English"] * 10))
            n_older_limited = int(
                math.ceil(float(row[column_indices["n_older_limited"]]) / max_values["n_older_limited"] * 10))
            n_all_limited = int(
                math.ceil(float(row[column_indices["n_all_limited"]]) / max_values["n_all_limited"] * 10))
            n_older_alone = int(
                math.ceil(float(row[column_indices["n_older_alone"]]) / max_values["n_older_alone"] * 10))
            prop_alone = int(math.ceil(float(row[column_indices["prop_alone"]]) / max_values["prop_alone"] * 10))

            new_feature['properties'] = {
                "geog_id": row[column_indices["LSOA_Code"]],
                "no_English": n_english if n_english > 0 else 1,
                "n_older_limited": n_older_limited if n_older_limited > 0 else 1,
                "n_all_limited": n_all_limited if n_all_limited > 0 else 1,
                "n_older_alone": n_older_alone if n_older_alone > 0 else 1,
                "prop_alone": prop_alone if prop_alone > 0 else 1
            }

            if lsoa_code in coordinates_map:
                new_feature['geometry'] = coordinates_map[lsoa_code]
            else:
                new_feature['geometry'] = {}

            demographics['features'].append(new_feature)
            id += 1

    return demographics


def create_demographics_geojson():
    with open('../data/lsoa_coordinates_map.json', 'r') as f:
        coordinates_map = json.load(f)

    demographics = build_demographics_geojson_from_csv(coordinates_map)

    with open('../data/demographics.geojson', 'w') as f:
        json.dump(demographics, f)


if __name__ == '__main__':
    create_demographics_geojson()
