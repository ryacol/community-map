# Infrastructure

The system's infrastructure is described with terraform and can be used 
to spin up a new environment or make changes to the infrastructure of 
an environmet.

# Directory structure

| Directory |  Description |
| --- | --- |
| out/                  | all artifacts of the build process |
| [doc/](doc)                  | Development documentation. |
| test/                 | Test configurations, test suites and associated resources |
| src/                  | all code created by the project |
| src/azure             | all cloud provisioning scripts for the project |
| src/azure/foundation  | Bootstrap scripts for cloud infrastructure |
| src/azure/project     | Scripts provisioning project-specific cloud resources |
| tools/                | tool configurations. Binaries for tools should be part of the provisioning configuration |
| tools/build/          | build system. Scripts and configuration of the build/test/package/deploy process |
| tools/ci/             | CI scripts, job and pipeline definitions |
 
### Requirements

You'll need to install various software to start building the
system's infrastructure.

| Software |  Installation guide |
| --- | --- |
| Azure CLI | [link](https://docs.microsoft.com/en-us/cli/azure/install-azure-cli?view=azure-cli-latest) |
| Terraform CLI | [link](https://www.terraform.io/downloads.html) |
| Ruby | ``$ brew install ruby`` |
|  | ```$ echo 'export PATH="/usr/local/opt/ruby/bin:$PATH"' >> ~/.\[bash_profile\|zshrc\]``` |
| Rake | [link](https://github.com/ruby/rake) |

## Azure CLI

Once installed, make sure to login to your Azure account.

Replace the `subscriptionId` in the terraform variables in [src/azure](src/azure)

## Terraform

After downloading Terraform unzip the binary, move it to a directory of 
preference and finally add the binary's path to PATH, i.e.:

```
$ mkdir ${HOME}/.terraform
$ nano .bash_profile

    export PATH="${PATH}:${HOME}/.terraform"
```

Verify the installation was successful by opening a new terminal session and checking that 
`terraform` is available by executing `terraform`.

## Tools 

### Az On Ruby

AZ on Ruby is basically a Ruby code that wraps together commands to the 
Terraform CLI and the Azure CLI.

You'll need to run the bundler before you can call any Rake tasks.

#### Build & Bundle

By simply running:

```
cd tool/az-on-ruby
bundle init
bundle install
```

Now you can execute or create your own tasks to execute commands of either CLI.

#### Build System Reference

Az on Ruby is mainly used via rake tasks, to quickly generate the documentation on the `out/doc/` directory just run:

```
cd infrastructure
rake doc:gaudi
```
