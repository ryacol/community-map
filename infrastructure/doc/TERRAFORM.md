# Terraform Conventions

There are two terraform configurations available in each project.

For configuration details refer to the [Azure Provider reference guide](https://www.terraform.io/docs/providers/azurerm/index.html)

## Foundation (bootstrap)

All relevant scripts are in [src/azure/foundation](../../src/azure/foundation)

The infrastructure configuration performs the minimum operations 
required for management of an Azure subscription via terraform.

It creates a resource group and a storage account within that group 
configured το terraform state blobs.

These resources are meant to be created once at the start of a project 
and because they are required by all later operations are kept separate.

This means that there is no managed state available and re-running this 
configuration will result in the re-creation of the foundation and will 
probably break the project-specific terraform configuration as well.

Read what terraform is about to create by running:

```
rake foundation:plan
```

And, apply all changes planed by terraform by simply running:

```
rake foundation:apply
```

Destroy what you created with:

```
rake foundation:destroy
```

## Project

All relevant scripts are in [src/azure/project](../../src/azure/project)

The project configuration is configured to maintain state in Azure using 
the storage account (terraformstorage) created in the bootstrap process.

Access to the [azurerm backend](https://www.terraform.io/docs/backends/types/azurerm.html) is configured with an access key.

The _access key_ is taken from the terraformstorage account settings.

![Access Keys](images/AccessKeys.png)

The backend configuration is __not__ commited into the repo.

When working from a local clone the configuration file can be generated with ```rake project:backend```.

Read what terraform is about to create by running:

```
rake project:plan
```

And, apply all changes planed by terraform by simply running:

```
rake project:apply
```

Destroy what you created with:

```
rake project:destroy
```