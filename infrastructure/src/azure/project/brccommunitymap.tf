# Configure the Azure Provider
provider "azurerm" {
  # whilst the `version` attribute is optional, we recommend pinning to a given version of the Provider
  version         = "=1.36.0"
  subscription_id = var.subscription
}

resource "azurerm_resource_group" "brc_resource_group" {
  location = "UK South"
  name     = "brc_resouce_group"
}

resource "azurerm_app_service_plan" "brc_app_service_plan" {
  name                = "brc-appserviceplan"
  location            = azurerm_resource_group.brc_resource_group.location
  resource_group_name = azurerm_resource_group.brc_resource_group.name

  sku {
    tier = "Standard"
    size = "S1"
  }
}

resource "azurerm_app_service" "brc_app_service" {
  name                = "community-map"
  location            = azurerm_resource_group.brc_resource_group.location
  resource_group_name = azurerm_resource_group.brc_resource_group.name
  app_service_plan_id = azurerm_app_service_plan.brc_app_service_plan.id

#  site_config {
#    linux_fx_version = "DOCKER|registry.gitlab.com/communitypresencemap/community-map/frontend:latest"
#    always_on        = "true"
#  }

#  storage_account {
#    name = "brc-map-tiles-blob"
#    share_name = "brc-map-sco"
#    account_name = var.app_storage_account_name
#    type = "AzureBlob"
#    access_key = "+6hW52Wed4FxBm9WuL5/d/zE4mGkq3qUm6yXNYNTZlLNjSkr4wqq2B9lf/+QMFXk1CcujxO0yoojUyNGBp6Huw=="
#    mount_path = "/data/"
#  }

  tags = {
      "approver"      = var.owner
      "environment"   = ""
      "managedBy"     = ""
      "nextReview"    = ""
      "operationsILA" = ""
      "owner"         = ""
      "project"       = var.project
      "projectILA"    = ""
      "purpose"       = ""
      "service"       = ""
      "serviceLevel"  = "noSLA",
      "creationDate"  = ""
  }
}

resource "azurerm_storage_account" "brc_storage_account" {
  access_tier                    = "Hot"
  account_kind                   = "BlobStorage"
  account_encryption_source      = "Microsoft.Storage"
  account_replication_type       = "LRS"
  account_tier                   = "Standard"
  enable_blob_encryption         = true
  enable_file_encryption         = true
  enable_https_traffic_only      = true
  is_hns_enabled                 = false
  location                       = "UK South"
  name                           = var.app_storage_account_name
  resource_group_name            = azurerm_resource_group.brc_resource_group.name
}

resource "azurerm_storage_container" "infrastructure_container" {
  name                  = "brc-community-map-tiles"
  storage_account_name  = azurerm_storage_account.brc_storage_account.name
  container_access_type = "private"
}