variable "project" {
  type        = string
  description = "The name of the project."
}

variable "storage_account_name" {
  type        = string
  description = "The name of the foundation's storage."
}

variable "foundation_resource_group_name" {
  type        = string
  description = "The name of the resource group created by terraform for the project's foundation."
}

variable "subscription" {
  type = string
  description = " The Azure subscription ID used by the project"
}

variable "owner" {
  type        = string
  description = "The name of the project."
}
